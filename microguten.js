'use strict';

exports.handler = (event, context, callback) => {
    var corpus = JSON.parse(require('fs').readFileSync(__dirname + '/corpus.json'));
    var pick = corpus[Math.floor(Math.random() * corpus.length)];
    callback(null, pick);
};
